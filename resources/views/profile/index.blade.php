@extends('layouts.master')

@section('judul')
Update Profile
@endsection

@section('content')
<form action="/profile/{{$detailProfile->id}}" method="POST">
    @csrf
    @method("PUT")


    <div class="form-group">
      <label >Username</label>
      <input type="text" class="form-control" value="{{$detailProfile->user->name}}" disabled>
    </div>

    <div class="form-group">
      <label >Email</label>
      <input type="text"  class="form-control" value="{{$detailProfile->user->email}}" disabled>
    </div>    

      <div class="form-group">
        <label >Nama</label>
        <input type="text" name="nama" class="form-control" value="{{$detailProfile->nama}}">
      </div>
      @error('nama')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror
  
      <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" id="" cols="30" rows="10">{{$detailProfile->alamat}}</textarea>    
      </div>
      @error('alamat')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <div class="form-group">
        <label >Nomor Telpon</label>
        <input type="number" name="telfon" class="form-control" value="{{$detailProfile->telfon}}">
      </div>
      @error('telfon')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

      
{{-- 
      <div class="form-group">
        <label >Jenis Kelamin</label>
        <select name="jeniskelamin" class="form-control">
          <option value="">--Jenis Kelamin--</option>
          <option value="">--Pria--</option>
          <option value="">--Wanita--</option>
        </select>
      </div>
      @error('jeniskelamin')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror --}}

      <div class="form-group">
        <label >Jenis Kelamin</label>
        <input type="text" name="jeniskelamin" class="form-control" value="{{$detailProfile->jeniskelamin}}">
      </div>
      @error('jeniskelamin')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

  


      <button type="submit" class="btn btn-primary">Submit</button>
      
      
    </form>

@endsection