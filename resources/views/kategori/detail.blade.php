@extends('layouts.master')

@section('judul')
<h3><u><br><br>DETAIL KATEGORI</u></h3>
@endsection

@section('content')
<h1>{{$kategori->nama_kategori}}</h1>
<p>{{$kategori->deskripsi}}</p>

<div class="row">
    @forelse ($kategori->produks as $item)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('image/'.$item->url_gambar)}}" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{$item->nama_produk}}</h5>
                <p class="card-text">{{$item->deskripsi}}</p>
                <a href="/produk/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Produk</a>
            </div>
        </div> 
    </div>
    @empty
     <h3>Tidak ada produk pada kategori ini</h3>
    @endforelse 
</div>
<br>
<a href="/kategori" class="btn btn-secondary btn-sm">Back</a>
@endsection