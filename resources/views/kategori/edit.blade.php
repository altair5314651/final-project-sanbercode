@extends('layouts.master')

@section('judul')
<h3><u><br><br>EDIT KATEGORI</u></h3>
@endsection

@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label >Nama Kategori</label>
      <input type="text" name="nama_kategori" value="{{$kategori->nama_kategori}}" class="form-control">
    </div>
    @error('nama_kategori')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Deskripsi</label>
      <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>    
    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
    
    
  </form>
@endsection