@extends('layouts.master')

@section('judul')
<h3><u><br><br>INPUT PRODUK</u></h3>
@endsection

@section('content')
<form action="/produk" method="POST" enctype="multipart/form-data">
  @csrf
    <div class="form-group">
      <label >Nama Produk</label>
      <input type="text" name="nama_produk" class="form-control">
    </div>
    @error('nama_produk')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Deskripsi</label>
      <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10"></textarea>    
    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror


    <div class="form-group">
        <label >Harga</label>
        <input type="text" name="harga" class="form-control">
    </div>
    @error('harga')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Gambar Produk</label>
        <input type="file" name="url_gambar" class="form-control">
    </div>
    @error('url_gambar')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Kategori</label>
        <br>
        <select name="kategori_id" class="form-control">
            <option value="">--Pilih Kategori--</option>
            @forelse ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
            @empty
                <option value="">Tidak ada data Kategori</option>
            @endforelse
        </select>  
        <br><br>
    </div>
    
    @error('deskripsi')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
       
</form>
@endsection

