@extends('layouts.master')

@section('judul')
<h3><u><br><br>Detail Produk</u></h3>
@endsection

@section('content')

    <br>
    <img class="card-img-top mx-auto d-block rounded" src="{{asset('image/'.$produk->url_gambar)}}" style="max-width: 50%" alt="Card image cap">
        <br>
        <h3>{{$produk->nama_produk}}</h3>
        <p class="card-text">Deskripsi : {{$produk->deskripsi}}</p>
        <p class="card-text">Harga : Rp{{$produk->harga}}</p>
        <p class="card-text">Kategori : {{$produk->kategori->nama_kategori}}</p>
    <br>
    @auth
    @forelse ($produk->keranjang as $item)
        <div class="media my-3 border p-3">
            <img src="{{asset('image/'.$produk->url_gambar)}}" class="mr-3" style="border-radius:50%" width="200px" alt="...">
            <div class="media-body">
                <h5 class="mt-0">{{$item->produk->nama_produk}}</h5>
                <p>{{$item->quantity}}</p>
            </div>
        </div>
    @empty
        <h4>Keranjang kosong</h4>
    @endforelse


    <hr>

    <h4>Tambah ke Keranjang</h4>

    <hr>

    <form action="/keranjang/{{$produk->id}}" method="post">
        @csrf
        <div class="form-group">
            <input type="number" name="quantity" class="form-control my-3" placeholder="Isi Jumlah">
        </div>
        @error('quantity')
            <div class="alert alert-danger">
                {{$messsage}}
            </div>
        @enderror
        <input type="submit" value=" Add to Cart ">
    </form>
    
    @endauth
    
    <hr>

    <a href="/produk" class="btn btn-secondary btn-block btn-sm">Kembali</a>
    <br>
  

@endsection