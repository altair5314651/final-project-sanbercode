@extends('layouts.master')

@section('judul')
<h3><u><br><br>List Produk</u></h3>
@endsection

@section('content')
@auth
<a href="/produk/create" class="btn btn-primary btn-sm mb-4">Add Product</a>    
@endauth

<div class="row">
    @forelse ($produk as $item)
        <div class="col-4">
            <div class="card">
                <img class="card-img-top" src="{{asset('image/'.$item->url_gambar)}}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{$item->nama_produk}}</h5>
                    <span class="badge badge-info">{{$item->kategori->nama_kategori}}</span>
                    <p class="card-text">{{$item->deskripsi}}</p>
                    <a href="/produk/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Produk</a>
                    @auth
                    <div class="row my-2">
                        <div class="col">
                            <a href="/produk/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                    
                        </div>
                        <div class="col">
                            <form action="produk/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">

                            </form>
                        </div>
                    </div>
                    @endauth
                    
                </div>
            </div> 
        </div>
              
    @empty
     <h2>.|Tidak ada Produk</h2>
    @endforelse


</div>


@endsection