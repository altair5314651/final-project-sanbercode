@extends('layouts.master')

@section('judul')
<h3><u><br><br>INPUT PRODUK</u></h3>
@endsection

@section('content')
<form action="/produk/{{$produk->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label >Nama Produk</label>
      <input type="text" name="nama_produk" value="{{$produk->nama_produk}}" class="form-control">
    </div>
    @error('nama_produk')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Deskripsi</label>
      <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10">{{$produk->deskripsi}}</textarea>    
    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror


    <div class="form-group">
        <label >Harga</label>
        <input type="text" name="harga" value="{{$produk->harga}}" class="form-control">
    </div>
    @error('harga')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Gambar Produk</label>
        <input type="file" name="url_gambar" class="form-control">
    </div>
    @error('url_gambar')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Kategori</label>
        <br>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse ($kategori as $item)
                @if ($item->id === $produk->kategori_id)
                <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                @else
                <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>    
                @endif
            @empty
                <option value="">Tidak ada data Kategori</option>
            @endforelse
        </select>  <br><br>
    </div>
    
    @error('deskripsi')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    

    <button type="submit" class="btn btn-primary">Submit</button>
       
</form>
@endsection

