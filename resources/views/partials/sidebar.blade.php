    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.html"><img src="{{asset('/template/img/core-img/zeroone.png')}}" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
            
        </div>

        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.html"><img src="{{asset('/template/img/core-img/zeroone.png')}}" alt=""></a>
            </div>
            <!-- Amado Nav -->
            <nav class="amado-nav text-white">
                <ul>
                    <li><a href="/">Home</a></li>
                    
                    <li><a href="/produk">Produk</a></li>
                    @auth
                    <li><a href="/profile">Profile</a></li>
                    <li><a href="/kategori">Kategori</a></li>
                    @endauth

                    {{-- <li><a href="#">Checkout</a></li> --}}
                </ul>
            </nav>
            <!-- Button Group -->
            <div class="amado-btn-group mt-30 mb-100">
                @guest
                    <a href="/login" class="btn amado-btn mb-15">Log in</a>
                    <a href="/register" class="btn amado-btn active">Register</a><br>
                @endguest

                @auth
                <a class="amado-btn-group mt-30 mb-100">
                    <a class="btn amado-btn bg-danger mb-15" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Log Out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
    
                @endauth
                


            </div>
            <!-- Cart Menu -->
            @auth
            <div class="cart-fav-search mb-100">
                <a href="/keranjang" class="cart-nav"><img src="{{asset('/template/img/core-img/cart.png')}}" alt=""> Keranjang </a>
                {{-- <a href="#" class="fav-nav"><img src="{{asset('/template/img/core-img/favorites.png')}}" alt=""> Favourite</a> --}}
                <a href="#" class="search-nav"><img src="{{asset('/template/img/core-img/search.png')}}" alt=""> Search</a>
            </div>    
            @endauth
            
            <!-- Social Button -->
            <div class="social-info d-flex justify-content-between">
                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </header>
        <!-- Header Area End -->