<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pelanggan extends Model
{
    use HasFactory;
    protected $table = 'pelanggan';
    protected $fillable = ['nama','alamat','telfon','jeniskelamin','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
