<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class kategoriController extends Controller
{
    public function createkategori()
    {
        return view('kategori.tambah');
    }

    public function storekategori(Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required',
            'deskripsi' => 'required'
        ]);

        DB::table('kategori')->insert([
            'nama_kategori' => $request['nama_kategori'],
            'deskripsi' => $request['deskripsi'],
        ]);

        return redirect('/kategori');
    }

    public function datakategori()
    {
        $kategori = DB::table('kategori')->get();

        return view('kategori.tampil', ['kategori'=>$kategori]);
    }

    public function showkategori($id)
    {
        $kategori =   Kategori::find($id);
        return view('kategori.detail', ['kategori'=>$kategori]);
    }

    public function editkategori($id)
    {
        $kategori =   DB::table('kategori')->where('id', $id)->first();

        return view('kategori.edit', ['kategori'=>$kategori]);        
    }

    public function updatekategori(Request $request, $id)
    {
        $request->validate([
            'nama_kategori' => 'required',
            'deskripsi' => 'required'
        ]);

        DB::table('kategori')
              ->where('id', $id)
              ->update
                (
                    [
                        'nama_kategori' => $request->nama_kategori,
                        'deskripsi' => $request->deskripsi
                    ],
                );
        
            return redirect('/kategori');
    }

    public function deletekategori($id)
    {
        DB::table('kategori')->where('id', $id)->delete();

        return redirect('/kategori');
    }
}
