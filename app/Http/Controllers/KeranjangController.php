<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Keranjang;
use Illuminate\Support\Facades\Auth;

class KeranjangController extends Controller
{
    public function tambah(Request $request, $id)
    {
        $request->validate([
            'quantity' => 'required',
        ]);

        $iduser = Auth::id();
         
        $keranjang = new Keranjang;
 
        $keranjang->user_id = $iduser;
        $keranjang->quantity = $request->quantity;
        $keranjang->produk_id = $id;
 
        $keranjang->save();

        return redirect('/produk/'. $id);
    }
}
