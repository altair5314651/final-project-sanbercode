<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Produk;
use File;

class newprodukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        $produk = Produk::get();
        return view('post.index', ['produk'=>$produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('post.create', ['kategori'=> $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'url_gambar' => 'required|image|mimes:jpg,jpeg,png',
            'kategori_id' => 'required'
        ]);
  
        $fileName = time().'.'.$request->url_gambar->extension();  
   
        $request->url_gambar->move(public_path('image'), $fileName);
   
 
        $produk = new Produk;
 
        $produk->nama_produk = $request->nama_produk;
        $produk->deskripsi = $request->deskripsi;
        $produk->harga = $request->harga;
        $produk->url_gambar = $fileName;
        $produk->kategori_id = $request->kategori_id;
 
        $produk->save();

        return redirect('/produk');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::find($id);

        return view('post.detail', ['produk'=>$produk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $kategori = Kategori::get();

        return view('post.update', ['produk'=>$produk, 'kategori'=>$kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'url_gambar' => 'image|mimes:jpg,jpeg,png',
            'kategori_id' => 'required'
        ]);

        $produk = Produk::find($id);

        if($request->has('url_gambar'))
        {
            $path = 'image/';
            File::delete($path, $produk->url_gambar);

            $fileName = time().'.'.$request->url_gambar->extension();
   
            $request->url_gambar->move(public_path('image'), $fileName);

            $produk->url_gambar=$fileName;

            $produk->save();
       
        }
 
        $produk->nama_produk = $request['nama_produk'];
        $produk->deskripsi = $request['deskripsi'];
        $produk->harga = $request['harga'];
        $produk->kategori_id = $request['kategori_id'];
        $produk->save();

        return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);

        $path = 'image/';
        File::delete($path, $produk->url_gambar);

 
        $produk->delete();
        return redirect('/produk');
    }
}
