<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pelanggan;
use Illuminate\Support\Facades\Auth;

class PelangganController extends Controller
{
    public function index(){
        $iduser = Auth::id();

        $detailProfile = pelanggan::where('user_id', $iduser)->first();

        return view('profile.index',['detailProfile'=>$detailProfile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'telfon' => 'required',
            'jeniskelamin' => 'required',
        ]);

                
        $pelanggan = pelanggan::find($id);
        
        $pelanggan->nama = $request->nama;
        $pelanggan->alamat = $request->alamat;
        $pelanggan->telfon = $request->telfon;
        $pelanggan->jeniskelamin = $request->jeniskelamin;
        
        $pelanggan->save();

        return redirect('/profile');
    }
}
