**## Final Project**

## Kelompok 1

## Anggota Kelompok

-   [Fikri Refo Julianto](https://gitlab.com/radianaltair).
-   [Reyhan Rizqi](https://gitlab.com/reyhanrizqi01).
-   [Rizal Alfikri](https://gitlab.com/rizalhimselff).

## Tema Project

Toko Online

## ERD

![ERD-Toko Online](public/ERD/ERD-TokoOnline.png)

### Link Video

-   [Video Demo Aplikasi](https://youtube.com/)
-   [Link Deploy](https://github.com/)
