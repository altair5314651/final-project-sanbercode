<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\kategoriController;
use App\Http\Controllers\newprodukController;
use App\Http\Controllers\PelangganController;
use App\Http\Controllers\KeranjangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/coba', function () {
    return view('coba');
});

Route::middleware(['auth'])->group(function () {
//CRUD kategori admin
//form tambah kategori admin

//Create
Route::get('/kategori/create', [kategoriController::class, 'createkategori']);
//Post
Route::post('/kategori', [kategoriController::class, 'storekategori']);
//Read
Route::get('/kategori', [kategoriController::class, 'datakategori']);
//Detail kategori by id
Route::get('/kategori/{kategori_id}', [kategoriController::class, 'showkategori']);
//Update
Route::get('/kategori/{kategori_id}/edit', [kategoriController::class, 'editkategori']);
//update database by id
Route::put('/kategori/{kategori_id}', [kategoriController::class, 'updatekategori']);
//Delete
//Delete by id
Route::delete('/kategori/{kategori_id}', [kategoriController::class, 'deletekategori']);

//Profile
Route::resource('/profile', PelangganController::class)->only(['index', 'update']);


//Keranjang
Route::post('/keranjang/{produk_id}',[KeranjangController::class, 'tambah']);


});

//CRUD post kategori
Route::resource('/produk', newprodukController::class);

Route::get('/keranjang', function () {
    return view('keranjang.index');
});


Route::get('/', function () {
    return view('coba');
});

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
